import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import PrivateLayout from '../../layout/PrivateLayout';
import { PageContent } from '../../components/common';
import { Archive, Userlyout } from '../../assets/icons';
import ModalLayout from '../../layout/ModalLayout';
import { Transactions } from '../../pages';
import Volunteer from '../../pages/Volunteer';

export const PRIVATE_ROUTES = [
  {
    key: 'volunteers',
    exact: true,
    text: 'Volunteers',
    icon: Userlyout,
    path: '/',
    routes: [
      {
        path: '/',
        title: 'volunteer.title',
        component: Volunteer.List
      }
    ],
    modals: [
      {
        path: '/create',
        component: Volunteer.Create,
        title: 'employee.modals.create'
      },
      {
        path: '/edit',
        component: Volunteer.Edit,
        title: 'employee.modals.edit'
      }
    ]
  },
  {
    key: 'transactions',
    exact: true,
    text: 'Transactions',
    icon: Archive,
    path: '/transactions',
    routes: [
      {
        path: '/',
        title: 'transaction.title',
        component: Transactions.List
      }
    ],
    modals: [
      {
        path: '/create',
        component: Transactions.Create,
        title: 'transaction.modals.create'
      },
      {
        path: '/edit',
        component: Transactions.Edit,
        title: 'transaction.modals.edit'
      }
    ]
  }
];

export const MODAL_LISTS = [...PRIVATE_ROUTES].reverse(({ path, modals }) => ({
  path,
  modals
}));

const PrivateRoutes = () => (
  <PrivateLayout>
    <ModalLayout />
    <Switch>
      {PRIVATE_ROUTES.flatMap(
        route =>
          route.routes &&
          route.routes.map(subRoute => ({
            ...subRoute,
            path: route.path + subRoute.path,
            exact: subRoute.path === '/'
          }))
      ).map(route => (
        <Route
          {...route}
          component={e => (
            <>
              <PageContent title={route.title} {...e} path={route.path} />
              <route.component {...e} />
            </>
          )}
          key={route.path}
        />
      ))}
      <Redirect to="/" />
    </Switch>
  </PrivateLayout>
);

export default PrivateRoutes;
