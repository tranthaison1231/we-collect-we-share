import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const StyledActionGroup = styled.div`
  .ant-btn {
    width: 95px;
    height: 28px;
    border-radius: 2px;
    line-height: 1.43;
    margin-right: 12px;
  }
`;
