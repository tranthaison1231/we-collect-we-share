import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown, Avatar, Badge, Icon } from 'antd';
import { connect } from 'react-redux';
import { TitleHeader } from '..';
import {
  logoutFirebaseAction,
  getCurrentUserFirebaseAction
} from '../../../redux/auth/actions';
import StyledHeader from './style';

const CustomHeader = ({
  logoutFirebase,
  currentUser,
  getCurrentUserFirebase
}) => {
  const profileMenu = [
    {
      key: 'profile',
      text: 'Profile',
      url: '#'
    }
  ];
  useEffect(() => {
    getCurrentUserFirebase();
  }, [getCurrentUserFirebase]);
  return (
    <>
      <StyledHeader>
        <TitleHeader />

        <div className="rightHeader">
          <Dropdown
            overlay={
              <Menu style={{ minWidth: '120px' }}>
                {profileMenu.map(menu => (
                  <Menu.Item key={menu.key}>
                    <a href={menu.url}>{menu.text}</a>
                  </Menu.Item>
                ))}
                <Menu.Divider />
                <Menu.Item
                  onClick={() => {
                    logoutFirebase();
                  }}
                  key="logout"
                >
                  Logout
                </Menu.Item>
              </Menu>
            }
            trigger={['click']}
          >
            <Avatar size="large" src={currentUser.photoURL} />
          </Dropdown>
          <div className="info">
            <p className="name"> {currentUser.displayName}</p>
          </div>
          <div style={{ marginRight: '30px' }}>
            <Badge count={5}>
              <Icon type="bell" style={{ fontSize: '25px' }} />
            </Badge>
          </div>
        </div>
      </StyledHeader>
    </>
  );
};

CustomHeader.propTypes = {
  logoutFirebase: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({
    photoURL: PropTypes.string,
    displayName: PropTypes.string
  }),
  getCurrentUserFirebase: PropTypes.func.isRequired
};

CustomHeader.defaultProps = {
  currentUser: {}
};

export default memo(
  connect(
    state => ({
      isAuthenticated: state.auth.isAuthenticated,
      currentUser: state.auth.data
    }),
    dispatch => ({
      logoutFirebase: () => dispatch(logoutFirebaseAction()),
      getCurrentUserFirebase: () => dispatch(getCurrentUserFirebaseAction())
    })
  )(CustomHeader)
);
