import React from 'react';
import i18n from 'i18next';
import PropTypes from 'prop-types';
import { Button, Icon, Select } from 'antd';
import debounce from 'lodash/debounce';
import SearchBarWrapper from './styles';
import iamIndex from '../../../algolia';

const { Option } = Select;

const SearchBar = ({ getFilteredData, onCancel, results, algoliaSearch }) => {
  const handleChange = value => {
    algoliaSearch({ index: iamIndex, query: value });
  };
  const handleSelect = value => {
    getFilteredData({ docID: value });
  };
  const handleCancel = e => {
    e.preventDefault();
    onCancel();
  };
  return (
    <SearchBarWrapper>
      <Select
        showSearch
        allowClear
        onSearch={debounce(handleChange, 500)}
        onSelect={handleSelect}
        showArrow={false}
        filterOption={false}
        defaultActiveFirstOption={false}
        notFoundContent={null}
        placeholder={i18n.t('searchBar.user')}
        suffixIcon={<Icon type="search" />}
      >
        {results.map(result => (
          <Option key={result.objectID}>{result.email}</Option>
        ))}
      </Select>
      <Button onClick={handleCancel}>
        <Icon type="close" />
      </Button>
    </SearchBarWrapper>
  );
};

SearchBar.propTypes = {
  getFilteredData: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  results: PropTypes.array.isRequired,
  algoliaSearch: PropTypes.func.isRequired,
};

export default SearchBar;
