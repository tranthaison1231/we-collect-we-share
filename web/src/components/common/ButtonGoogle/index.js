import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import i18n from 'i18next';
import firebase from 'firebase';
import { connect } from 'react-redux';
import StyledButton from './style';
import { GoogleIcon } from '../../../assets/icons';
import { loginFirebaseAction } from '../../../redux/auth/actions';

const provider = new firebase.auth.GoogleAuthProvider();

const ButtonGoogle = ({ loginFirebase }) => {
  const handleClick = () => {
    loginFirebase(provider);
  };
  return (
    <StyledButton onClick={handleClick}>
      <Icon component={GoogleIcon} />
      {i18n.t('login.google')}
    </StyledButton>
  );
};

ButtonGoogle.propTypes = {
  loginFirebase: PropTypes.func.isRequired,
};

export default connect(
  null,
  dispatch => ({
    loginFirebase: payload => {
      dispatch(loginFirebaseAction(payload));
    },
  })
)(ButtonGoogle);
