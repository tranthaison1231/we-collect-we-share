import React from 'react';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Button } from 'antd';
import { connect } from 'react-redux';
import { useLocation } from 'react-router';
import PageTitle from '../PageTitle';
import PageContentWrapper from './styles';
import {
  showModalAction,
  showModalWithItemAction
} from '../../../redux/modal/actions';

const PageContent = ({
  title,
  children,
  showModal,
  match,
  showModalWithItem,
  path
}) => {
  const location = useLocation();

  const showModalCreatePage = () => {
    const routeCreate = `${location.pathname}/create`;
    showModal(routeCreate);
  };
  const showMoDalCreatePageWithItem = () => {
    const routeCreate = `${location.pathname}/create`;
    showModalWithItem({ route: routeCreate, item: match });
  };

  const BUTTON = {
    '/answers/': null,
    '/answers/:id': <Button type="primary">Download</Button>,
    Default: (
      <Button
        className="button"
        type="primary"
        onClick={
          match.params.id ? showMoDalCreatePageWithItem : showModalCreatePage
        }
      >
        <span>{i18n.t('button.create')}</span>
      </Button>
    )
  };

  return (
    <PageContentWrapper>
      <header className="page-header">
        <PageTitle>{i18n.t(title)}</PageTitle>
        {BUTTON[path] || BUTTON.Default}
      </header>
      <div className="mainContent">{children}</div>
    </PageContentWrapper>
  );
};

PageContent.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.any,
  showModal: PropTypes.func.isRequired,
  showModalWithItem: PropTypes.func.isRequired,
  match: PropTypes.object,
  path: PropTypes.string
};

PageContent.defaultProps = {
  match: {}
};

const mapDispatchToProps = dispatch => ({
  showModal: data => dispatch(showModalAction(data)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params))
});

export default connect(
  null,
  mapDispatchToProps
)(PageContent);
