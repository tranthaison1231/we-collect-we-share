import styled from 'styled-components';

export default styled.div`
  .pagingButtons {
    width: fit-content;
    margin: auto;
    padding-top: 24px;
  }
  .email-field {
    margin-left: 10px;
  }
  .ant-table-thead > tr > th {
    color: #808385;
    text-transform: uppercase;
    font-size: 14px;
    font-family: 'Barlow-regular', sans-serif;
  }
  .ant-table-tbody > tr > td {
    padding: 20px 34.1px;
  }

  .ant-table-row {
    background-color: #fff;
    color: #071924;
  }
`;
