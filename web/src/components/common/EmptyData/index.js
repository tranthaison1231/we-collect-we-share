import React from 'react';
import PropTypes from 'prop-types';
import EmptyDataWrapper from './style';

const EmptyData = ({ icon, text }) => (
  <EmptyDataWrapper>
    <div className="content">
      <div className="icon">
        <i className={icon || 'anticon-ic-face-smile'} />
      </div>
      <div className="text">
        <p>{text || 'Chưa có thông tin để hiển thị'}</p>
      </div>
    </div>
  </EmptyDataWrapper>
);

EmptyData.propTypes = {
  icon: PropTypes.string,
  text: PropTypes.string,
};
export default EmptyData;
