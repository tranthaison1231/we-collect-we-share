import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const TransactionsTypes = makeConstantCreator(
  'GET_ALL_TRANSACTIONS',
  'GET_TRANSACTIONS_SUCCESS',
  'GET_TRANSACTIONS_FAIL',
  'GET_NEXT_TRANSACTIONS',
  'GET_PREVIOUS_TRANSACTIONS',
  'CREATE_TRANSACTION',
  'CREATE_TRANSACTION_SUCCESS',
  'CREATE_TRANSACTION_FAIL',
  'DELETE_TRANSACTION',
  'DELETE_TRANSACTION_SUCCESS',
  'DELETE_TRANSACTION_FAIL',
  'EDIT_TRANSACTION',
  'EDIT_TRANSACTION_SUCCESS',
  'EDIT_TRANSACTION_FAIL',
  'UNSUBSCRIBE',
  'UPDATE_STATUS',
  'UPDATE_STATUS_SUCCESS',
  'UPDATE_NEW_STATUS',
  'UPDATE_NEW_STATUS_SUCCESS'
);

export const updateStatusSuccessAction = ({ id }) =>
  makeActionCreator(TransactionsTypes.UPDATE_STATUS_SUCCESS, { id });

export const createTransactionAction = ({ transaction }) =>
  makeActionCreator(TransactionsTypes.CREATE_TRANSACTION, {
    transaction
  });

export const createTransactionSuccessAction = () =>
  makeActionCreator(TransactionsTypes.CREATE_TRANSACTION_SUCCESS);

export const createTransactionFailAction = () =>
  makeActionCreator(TransactionsTypes.CREATE_TRANSACTION_FAIL);

export const deleteTransactionAction = ({ id }) =>
  makeActionCreator(TransactionsTypes.DELETE_TRANSACTION, { id });

export const updateStatusAction = ({ idVolunteer, idTransaction }) =>
  makeActionCreator(TransactionsTypes.UPDATE_STATUS, {
    idVolunteer,
    idTransaction
  });
export const updateNewStatusSuccessAction = ({id}) => 
  makeActionCreator(TransactionsTypes.UPDATE_NEW_STATUS_SUCCESS, {id})

export const updateNewStatusAction = ({status, idTransaction}) => 
  makeActionCreator(TransactionsTypes.UPDATE_NEW_STATUS, {
    status,
    idTransaction
  })

export const deleteTransactionSuccessAction = ({ id }) =>
  makeActionCreator(TransactionsTypes.DELETE_TRANSACTION_SUCCESS, { id });

export const deleteTransactionFailAction = ({ error }) =>
  makeActionCreator(TransactionsTypes.DELETE_TRANSACTION_FAIL, { error });

export const editTransactionAction = ({ id, transaction }) =>
  makeActionCreator(TransactionsTypes.EDIT_TRANSACTION, {
    id,
    transaction
  });

export const editTransactionSuccessAction = ({ id, transaction }) =>
  makeActionCreator(TransactionsTypes.EDIT_TRANSACTION_SUCCESS, {
    id,
    transaction
  });

export const editTransactionFailAction = ({ error }) =>
  makeActionCreator(TransactionsTypes.EDIT_TRANSACTION_FAIL, { error });

export const getAllTransactionsAction = () =>
  makeActionCreator(TransactionsTypes.GET_ALL_TRANSACTIONS);

export const getTransactionsSuccessAction = ({ transactions }) =>
  makeActionCreator(TransactionsTypes.GET_TRANSACTIONS_SUCCESS, {
    transactions
  });

export const getTransactionsFailAction = ({ error }) =>
  makeActionCreator(TransactionsTypes.GET_TRANSACTIONS_FAIL, { error });

export const unSubscribeAction = () =>
  makeActionCreator(TransactionsTypes.UNSUBSCRIBE);
