import { makeReducerCreator } from '../../utils/reduxUtils';
import { TransactionsTypes } from './actions';

export const initialState = {
  transactions: [],
  error: null
};

const createTransactionSuccess = state => ({
  ...state,
  error: null
});

const updateStatusSuccess = (state, action) => ({
  ...state,
  transactions: state.transactions.map(transaction =>
    transaction.id === action.id
      ? { ...transaction, status: 'pending' }
      : transaction
  )
});

const createTransactionFail = (state, action) => ({
  ...state,
  error: action.error
});

const deleteTransactionSuccess = (state, action) => ({
  ...state,
  error: null,
  TRANSACTIONsByPage: state.TRANSACTIONsByPage.filter(
    TRANSACTION => TRANSACTION.id !== action.id
  )
});

const deleteTransactionFail = (state, action) => ({
  ...state,
  error: action.error
});

const editTransactionSuccess = (state, action) => ({
  ...state,
  error: null,
  TRANSACTIONsByPage: state.TRANSACTIONsByPage.map(TRANSACTION =>
    TRANSACTION.id === action.id
      ? { ...TRANSACTION, role: action.TRANSACTION.role }
      : TRANSACTION
  )
});

const editTransactionFail = (state, action) => ({
  ...state,
  error: action.error
});

const getTransactionsSuccess = (state, action) => ({
  ...state,
  transactions: action.transactions
});

const getTransactionFail = (state, action) => ({
  ...state,
  error: action.error
});

const transactions = makeReducerCreator(initialState, {
  [TransactionsTypes.CREATE_TRANSACTION_FAIL]: createTransactionFail,
  [TransactionsTypes.CREATE_TRANSACTION_SUCCESS]: createTransactionSuccess,
  [TransactionsTypes.DELETE_TRANSACTION_FAIL]: deleteTransactionFail,
  [TransactionsTypes.DELETE_TRANSACTION_SUCCESS]: deleteTransactionSuccess,
  [TransactionsTypes.EDIT_TRANSACTION_FAIL]: editTransactionFail,
  [TransactionsTypes.EDIT_TRANSACTION_SUCCESS]: editTransactionSuccess,
  [TransactionsTypes.GET_TRANSACTIONS_FAIL]: getTransactionFail,
  [TransactionsTypes.GET_TRANSACTIONS_SUCCESS]: getTransactionsSuccess,
  [TransactionsTypes.UPDATE_STATUS_SUCCESS]: updateStatusSuccess
});

export default transactions;
