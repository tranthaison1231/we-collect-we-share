import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import { notification } from 'antd';
import firebase from 'firebase';
import {
  TransactionsTypes,
  createTransactionFailAction,
  deleteTransactionFailAction,
  editTransactionFailAction,
  getTransactionsFailAction,
  getTransactionsSuccessAction,
  deleteTransactionSuccessAction,
  updateStatusSuccessAction,
  updateNewStatusSuccessAction
} from './actions';

function* updateStatusSaga({ idVolunteer, idTransaction }) {
  try {
    yield firebase
      .firestore()
      .collection('transactions')
      .doc(idTransaction)
      .update({
        status: 'processing',
        volunteerId: idVolunteer
      });
    yield put(updateStatusSuccessAction({ idTransaction }));
  } catch (e) {
    console.error(e);
  }
}

function* updateNewStatusSaga({ status, idTransaction }) {
  try {
    yield firebase
      .firestore()
      .collection('transactions')
      .doc(idTransaction)
      .update({
        status
      });
  } catch (e) {
    console.error(e);
  }
}

function* createTransactionSaga({ transaction }) {
  try {
    yield firebase
      .firestore()
      .collection('transactions')
      .add(transaction);
  } catch (e) {
    yield put(createTransactionFailAction(e));
  }
}

function* editTransactionSaga({ id, transaction }) {
  try {
    yield firebase
      .firestore()
      .collection('transactions')
      .doc(id)
      .update(transaction);
  } catch (e) {
    yield put(editTransactionFailAction(e));
  }
}

function* deleteTransactionSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('transactions')
      .doc(id)
      .delete();
    yield put(deleteTransactionSuccessAction({ id }));
  } catch (e) {
    yield put(deleteTransactionFailAction(e));
  }
}

let channel = null;

function* getAllTransactionsSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('transactions')
        .orderBy('giverName')
        .onSnapshot(docs => {
          docs.docChanges().forEach(change => {
            change.type === 'modified' &&
              change.doc.data().status === 'processing' &&
              notification.success({
                message: 'Volunteer assigned',
                description: `${
                  change.doc.data().giverName
                }'s case has been taken`
              });
            change.type === 'added' &&
              notification.success({
                message: 'Have a new one',
                description: `${
                  change.doc.data().giverName
                }'s case has been created`
              });
          });
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getTransactionsSuccessAction({ transactions: responseData }));
    }
  } catch (e) {
    yield put(getTransactionsFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(TransactionsTypes.CREATE_TRANSACTION, createTransactionSaga),
  takeEvery(TransactionsTypes.DELETE_TRANSACTION, deleteTransactionSaga),
  takeEvery(TransactionsTypes.EDIT_TRANSACTION, editTransactionSaga),
  takeEvery(TransactionsTypes.GET_ALL_TRANSACTIONS, getAllTransactionsSaga),
  takeEvery(TransactionsTypes.UNSUBSCRIBE, unSubscribeSaga),
  takeEvery(TransactionsTypes.UPDATE_STATUS, updateStatusSaga),
  takeEvery(TransactionsTypes.UPDATE_NEW_STATUS, updateNewStatusSaga)
];
