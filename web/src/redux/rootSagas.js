import { all } from 'redux-saga/effects';
import authSaga from './auth/sagas';
import firebaseImg from './firebaseImg/sagas';
import transactionsSaga from './transactions/sagas';
import volunteersSaga from './volunteers/sagas';

export default function* root() {
  yield all([
    ...authSaga,
    ...firebaseImg,
    ...transactionsSaga,
    ...volunteersSaga
  ]);
}
