import { takeEvery, put, call } from 'redux-saga/effects';
import firebase from 'firebase';
import { onAuthStateChanged } from '../../firebase';
import {
  loginFirebaseAction,
  logoutFirebaseAction,
  getCurrentUserFirebaseAction,
  loginSuccessAction,
  loginFailureAction,
  getCurentUserSuccessAction,
  logoutAction,
  getCurentUserFailureAction
} from './actions';

function* loginFirebaseSaga({ payload }) {
  try {
    const result = yield firebase.auth().signInWithPopup(payload);
    const sessionToken = result.credential.accessToken;
    localStorage.setItem('sessionToken', sessionToken);
    yield put(loginSuccessAction(result.user));
    yield put(
      getCurentUserSuccessAction({
        user: result.user
      })
    );
  } catch (e) {
    console.error(e.message);
    yield put(loginFailureAction(e));
  }
}

function* logoutFirebaseSaga() {
  try {
    yield firebase.auth().signOut();
    localStorage.clear('sessionToken');
    localStorage.clear('fullName');
    localStorage.clear('id');
    yield put(logoutAction());
  } catch (e) {
    console.error(e.message);
  }
}

function* getCurrentUserFirebaseSaga() {
  try {
    const user = yield call(onAuthStateChanged);
    const idToken = yield user.getIdTokenResult(true);
    yield put(
      getCurentUserSuccessAction({ user, admin: idToken.claims.admin })
    );
  } catch (e) {
    console.error(e.message);
    yield put(getCurentUserFailureAction(e));
  }
}

export default [
  takeEvery(loginFirebaseAction.type, loginFirebaseSaga),
  takeEvery(logoutFirebaseAction.type, logoutFirebaseSaga),
  takeEvery(getCurrentUserFirebaseAction.type, getCurrentUserFirebaseSaga)
];
