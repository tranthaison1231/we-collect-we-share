import { makeReducerCreator } from '../../utils/reduxUtils';
import { VolunteersTypes } from './actions';

export const initialState = {
  volunteers: [],
  error: null
};

const createVolunteerSuccess = state => ({
  ...state,
  error: null
});

const createVolunteerFail = (state, action) => ({
  ...state,
  error: action.error
});

const deleteVolunteerSuccess = (state, action) => ({
  ...state,
  error: null,
  volunteersByPage: state.volunteersByPage.filter(
    volunteer => volunteer.id !== action.id
  )
});

const deleteVoluntterFail = (state, action) => ({
  ...state,
  error: action.error
});

const editVolunteerSuccess = (state, action) => ({
  ...state,
  error: null,
  volunteersByPage: state.volunteersByPage.map(volunteer =>
    volunteer.id === action.id
      ? { ...volunteer, role: action.volunteer.role }
      : volunteer
  )
});

const editVolunteerFail = (state, action) => ({
  ...state,
  error: action.error
});

const getVolunteersSuccess = (state, action) => ({
  ...state,
  volunteers: action.volunteers
});

const getVolunteersFail = (state, action) => ({
  ...state,
  error: action.error
});

const volunteers = makeReducerCreator(initialState, {
  [VolunteersTypes.CREATE_VOLUNTEER_FAIL]: createVolunteerFail,
  [VolunteersTypes.CREATE_VOLUNTEER_SUCCESS]: createVolunteerSuccess,
  [VolunteersTypes.DELETE_VOLUNTEER_FAIL]: deleteVoluntterFail,
  [VolunteersTypes.DELETE_VOLUNTEER_SUCCESS]: deleteVolunteerSuccess,
  [VolunteersTypes.EDIT_VOLUNTEER_FAIL]: editVolunteerFail,
  [VolunteersTypes.EDIT_VOLUNTEER_SUCCESS]: editVolunteerSuccess,
  [VolunteersTypes.GET_VOLUNTEERS_FAIL]: getVolunteersFail,
  [VolunteersTypes.GET_VOLUNTEERS_SUCCESS]: getVolunteersSuccess
});

export default volunteers;
