import { connectRouter } from 'connected-react-router';
import { auth } from './auth/reducer';
import modal from './modal/reducer';
import loading from './loading/reducer';
import pagingButtons from './pagingButtons/reducer';
import searchBar from './searchBar/reducer';
import volunteers from './volunteers/reducer';
import transactions from './transactions/reducer';

export default history => ({
  router: connectRouter(history),
  auth,
  loading,
  modal,
  pagingButtons,
  searchBar,
  volunteers,
  transactions
});
