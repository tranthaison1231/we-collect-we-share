import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import {
  Icon,
  Dropdown,
  Menu,
  Tabs,
  Table,
  Tag,
  Select,
  Input,
  notification,
  Card,
  Col,
  Row
} from 'antd';
import { withGoogleMap, withScriptjs, GoogleMap } from 'react-google-maps';
import styled from 'styled-components';
import MarkerUI from '../../components/Map/Marker';
import {
  deleteTransactionAction,
  getAllTransactionsAction,
  unSubscribeAction,
  updateStatusAction,
  updateNewStatusAction
} from '../../redux/transactions/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import TableWrapper from '../../components/common/CustomTable/style';
import { getAllVolunteersAction } from '../../redux/volunteers/actions';
const { TabPane } = Tabs;

const { Option } = Select;

const { Search } = Input;

const openNotification = () => {
  notification.open({
    message: 'Notification Title',
    description:
      'This is the content of the notification. This is the content of the notification. This is the content of the notification.'
  });
};

const StyledSelect = styled(Select)`
  .ant-select-selection {
    background: ${props =>
      props.value === 'pending'
        ? '#fff1f0'
        : props.value === 'processing'
        ? '#e6f7ff'
        : '#00800045'};
    color: ${props =>
      props.value === 'pending'
        ? '#f5222d'
        : props.value === 'processing'
        ? '#1890ff'
        : '#52c41a'};
  }
`;

const TransactionContainer = ({
  getAllVolunteers,
  showModalWithItem,
  deleteTransaction,
  getAllTransactions,
  unSubscribe,
  getImageUrl,
  updateStatus,
  updateNewStatus
}) => {
  useEffect(() => {
    getAllTransactions();
    getAllVolunteers();
    return () => {
      unSubscribe();
    };
  }, [getAllTransactions, getAllVolunteers, unSubscribe]);
  const { transactions } = useSelector(state => state.transactions);
  const { volunteers } = useSelector(state => state.volunteers);

  const onSelectItem = (idVolunteer, idTransaction) => {
    updateStatus({ idVolunteer, idTransaction });
  };

  const onSelectStatus = (status, idTransaction) => {
    updateNewStatus({ status, idTransaction });
  };

  const gotoEditPage = record => {
    const route = '/transactions/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDelete = record => {
    deleteTransaction({ id: record.id });
  };
  const LocationMap = withScriptjs(
    withGoogleMap(props => (
      <GoogleMap
        defaultCenter={{ lat: 16.077348, lng: 108.229293 }}
        defaultZoom={15}
      >
        {props.markers.map(marker => (
          <MarkerUI key={marker.volunteerId} marker={marker} />
        ))}
      </GoogleMap>
    ))
  );
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage
    }
  ];
  const columns = [
    {
      title: i18n.t('table.title.description'),
      dataIndex: 'description',
      key: 'description',
      render: (description, row) => {
        return (
          <span>
            <div> {description} </div>
            <div style={{ display: 'flex', width: '160px', flexWrap: 'wrap' }}>
              {row.imageURL &&
                row.imageURL.slice(0, 3).map(e => (
                  <img
                    style={{
                      width: '50px',
                      height: '50px',
                      marginRight: '3px',
                      marginBottom: '3px'
                    }}
                    src={e}
                  />
                ))}
            </div>
          </span>
        );
      }
    },
    {
      title: i18n.t('table.title.giverName'),
      dataIndex: 'giverName',
      key: 'giverName',
      render: (name, row) => (
        <span>
          <span className="email-field">{name}</span>
        </span>
      )
    },
    {
      title: i18n.t('table.title.address'),
      dataIndex: 'address',
      key: 'address'
    },
    {
      title: i18n.t('table.title.date'),
      dataIndex: 'transactionTime',
      render: (transactionTime, row) => (
        <span>{new Date(transactionTime * 1000).toLocaleString()}</span>
      ),

      key: 'transactionTime'
    },
    {
      title: i18n.t('table.title.phoneNumber'),
      dataIndex: 'phoneNumber',
      key: 'phoneNumber'
    },

    {
      title: i18n.t('table.title.volunteer'),
      dataIndex: 'volunteerId',
      key: 'volunteerId',
      render: (id, row) => {
        return (
          <span>
            <Select
              value={id}
              style={{ width: 120, margin: '0 10px 0 0' }}
              onSelect={id => onSelectItem(id, row.id)}
            >
              {volunteers &&
                volunteers.map(e => <Option value={e.id}>{e.name}</Option>)}
            </Select>
          </span>
        );
      }
    },
    {
      title: i18n.t('table.title.status'),
      dataIndex: 'status',
      key: 'status',
      render: (status, row) => {
        return (
          <span>
            <StyledSelect
              value={status}
              style={{ width: 120, margin: '0 10px 0 0' }}
              onSelect={status => onSelectStatus(status, row.id)}
            >
              <Option
                style={{ background: '#fff1f0', color: 'red' }}
                value="pending"
              >
                Pending
              </Option>
              <Option
                style={{ background: '#e6f7ff', color: '#1890ff' }}
                value="processing"
              >
                Processing
              </Option>
              <Option
                style={{ background: '#00800045', color: '#52c41a' }}
                value="success"
              >
                Success
              </Option>
            </StyledSelect>
            {/* {!status && (
              <Tag style={{ padding: '4px' }} color="red">
                Pending
              </Tag>
            )}
            {status === 'processing' && (
              <Tag style={{ padding: '4px' }} color="blue">
                Processing
              </Tag>
            )}
            {status === 'success' && (
              <Tag style={{ padding: '4px' }} color="green">
                Success
              </Tag>
            )} */}
          </span>
        );
      }
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      )
    }
  ];
  const [filter, setFilter] = useState('all');
  return (
    <Tabs
      defaultActiveKey="1"
      tabPosition="top"
      tabBarStyle={{
        display: 'flex',
        justifyContent: 'center'
      }}
      size="large"
    >
      <TabPane
        tab={
          <span>
            <Icon type="unordered-list" />
            LIST
          </span>
        }
        key="1"
      >
        <Row gutter={30}>
          <Col span={8}>
            <Search
              placeholder="Search"
              enterButton="Search"
              style={{ width: '100%', marginBottom: '10px', marginTop: '10px' }}
              onSearch={value => console.log(value)}
            />
          </Col>
          <Col span={8}>
            <Select
              onChange={e => setFilter(e)}
              showSearch
              defaultValue="all"
              style={{ width: 300, marginTop: '10px' }}
              placeholder="Filter Status"
            >
              <Option value="pending">Pending</Option>
              <Option value="processing">Processing</Option>
              <Option value="success">Success</Option>
              <Option value="all">All</Option>
            </Select>
          </Col>
        </Row>
        <Row gutter={30} style={{ marginBottom: '10px' }}>
          <Col span={8}>
            <Card
              title="TOTAL PENDING"
              style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                fontSize: '33px',
                background: '#fff1f0',
                color: '#f5222d'
              }}
            >
              {transactions.filter(e => e.status === 'pending').length}
            </Card>
          </Col>
          <Col span={8}>
            <Card
              title="TOTAL PROCESSING"
              style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                fontSize: '33px',
                background: '#e6f7ff',
                color: '#1890ff'
              }}
            >
              {transactions.filter(e => e.status === 'processing').length}
            </Card>
          </Col>
          <Col span={8}>
            <Card
              title="TOTAL SUCCESS"
              style={{
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                fontSize: '33px',
                background: '#f6ffed',
                color: '#52c41a'
              }}
            >
              {transactions.filter(e => e.status === 'success').length}
            </Card>
          </Col>
        </Row>
        <TableWrapper>
          <Table
            pagination={{ pageSize: 4 }}
            bordered
            columns={columns}
            dataSource={
              filter === 'all'
                ? transactions
                : transactions.filter(e => e.status === filter)
            }
          />
        </TableWrapper>
      </TabPane>
      <TabPane
        tab={
          <span>
            <Icon type="global" />
            MAP
          </span>
        }
        key="2"
      >
        <LocationMap
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGZOhb6qWmy1PLYJrLmtBho18Vasw0C_U&v=3.exp&libraries=,drawing"
          containerElement={<div style={{ height: `70vh`, width: '100%' }} />}
          mapElement={<div style={{ height: `100%` }} />}
          loadingElement={<div style={{ height: '100%' }} />}
          markers={transactions}
        />
      </TabPane>
    </Tabs>
  );
};

TransactionContainer.propTypes = {
  employeesByPage: PropTypes.array,
  deleteEmployee: PropTypes.func,
  showModalWithItem: PropTypes.func,
  getAllTransactions: PropTypes.func,
  unSubscribe: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
  deleteTransaction: params => dispatch(deleteTransactionAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getAllTransactions: () => dispatch(getAllTransactionsAction()),
  getAllVolunteers: () => dispatch(getAllVolunteersAction()),
  updateStatus: params => dispatch(updateStatusAction(params)),
  unSubscribe: () => dispatch(unSubscribeAction()),
  updateNewStatus: params => dispatch(updateNewStatusAction(params))
});

export default connect(
  null,
  mapDispatchToProps
)(TransactionContainer);
