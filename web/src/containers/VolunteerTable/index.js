import React, { useEffect } from 'react';
import { connect, useSelector } from 'react-redux';
import i18n from 'i18next';
import { Icon, Dropdown, Menu, Table } from 'antd';
import {
  deleteVolunteerAction,
  getAllVolunteersAction,
  unSubscribeAction
} from '../../redux/volunteers/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import TableWrapper from '../../components/common/CustomTable/style';

const VolunteerContainer = ({
  showModalWithItem,
  deleteVolunteer,
  getAllVolunteers,
  getPreviousVolunteers,
  getNextVolunteers,
  lastVolunteer,
  firstVolunteer,
  disableNext,
  disablePrev,
  unSubscribe,
  getImageUrl
}) => {
  useEffect(() => {
    getAllVolunteers();
    return () => {
      unSubscribe();
    };
  }, [getAllVolunteers, unSubscribe]);
  const { volunteers } = useSelector(state => state.volunteers);
  const gotoEditPage = record => {
    const route = '/volunteers/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDelete = record => {
    deleteVolunteer({ id: record.id });
  };
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage
    }
  ];
  const columns = [
    {
      title: i18n.t('table.title.name'),
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: i18n.t('table.title.phoneNumber'),
      dataIndex: 'phoneNumber',
      key: 'phoneNumber'
    },
    {
      title: i18n.t('table.title.address'),
      dataIndex: 'address',
      key: 'address'
    },
    {
      title: i18n.t('table.title.birthday'),
      dataIndex: 'birthDay',
      render: birthDay => (
        <span>{new Date(birthDay).toLocaleDateString()}</span>
      ),
      key: 'birthDay'
    },
    {
      title: i18n.t('table.title.email'),
      dataIndex: 'email',
      key: 'email'
    },
    {
      title: i18n.t('table.title.achievement'),
      dataIndex: 'achievement',
      key: 'achievement',
      render: (id, row) => {
        return (
          <span>
            <Icon style={{marginRight: '5px', marginLeft: '25px'}} type="gift"/>
            {Math.floor(Math.random() * 10)}
          </span>
        )
      }
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      )
    }
  ];
  return (
    <>
      <TableWrapper>
        <Table bordered columns={columns} dataSource={volunteers} />
      </TableWrapper>
    </>
  );
};

const mapDispatchToProps = dispatch => ({
  deleteVolunteer: params => dispatch(deleteVolunteerAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getAllVolunteers: () => dispatch(getAllVolunteersAction()),
  unSubscribe: () => dispatch(unSubscribeAction())
});

export default connect(
  null,
  mapDispatchToProps
)(VolunteerContainer);
