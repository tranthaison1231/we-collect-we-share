import React from 'react';
import i18n from 'i18next';
import { ButtonGoogle } from '../../components/common';
import { withAuthen } from '../../hocs';

const Login = () => {
  return (
    <>
      <p className="loginHeader">{i18n.t('login.header')}</p>
      <ButtonGoogle />
    </>
  );
};

export default withAuthen(Login);
