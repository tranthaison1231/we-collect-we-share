export const STATUS = [
  {
    data: 'pending',
    color: 'red',
  },
  {
    data: 'processing',
    color: 'blue',
  },
  {
    data: 'success',
    color: 'green',
  },
];