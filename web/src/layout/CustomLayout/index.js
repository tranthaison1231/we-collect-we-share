import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import CustomLayoutWrapper from './styles';

const CustomLayout = ({ children }) => {
  return (
    <CustomLayoutWrapper>
      <Layout className="layout">
        <div className="main-content">{children}</div>
      </Layout>
    </CustomLayoutWrapper>
  );
};

CustomLayout.propTypes = {
  children: PropTypes.any,
};

export default CustomLayout;
