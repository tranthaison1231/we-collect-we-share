import Create from './Create';
import Edit from './Edit';
import { TransactionsTable as List } from '../../containers';

export default { Create, Edit, List };
