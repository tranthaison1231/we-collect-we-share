import React, { useEffect } from 'react';
import { Form, Button, Input, Upload, Icon, DatePicker, Select } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  closeModalAction,
  saveDraftAction
} from '../../../redux/modal/actions';
import { uploadImageAction } from '../../../redux/firebaseImg/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';
import { getAllTransactionsAction } from '../../../redux/transactions/actions';

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  recordData,
  editEmployee,
  id,
  draft,
  closeModal,
  saveDraft,
  uploadImage,
  volunteers,
  getAllVolunteers
}) => {
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        const birthday = new Date(values.birthday).getTime();
        const dayInCompany = new Date(values.dayInCompany).getTime();
        editEmployee({
          id: recordData.id,
          employee: {
            giverName: values.name,
            job: values.job,
            email: values.email,
            birthday,
            dayInCompany,
            imgName: values.profilePic.file
              ? values.profilePic.file.uid
              : values.profilePic.uid
          }
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    getAllVolunteers();
  }, [getAllVolunteers]);
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  const handleUpload = ({ onError, onSuccess, file }) => {
    try {
      uploadImage(file, onSuccess);
    } catch (error) {
      onError(error);
    }
  };
  return (
    <Form onSubmit={onSubmit} className="login-form">
      <Form.Item label="GiverName" colon={false}>
        {getFieldDecorator(
          'giverName',
          {
            initialValue: recordData.giverName || null
          },
          {
            rules: [{ required: true, message: 'Please enter role' }]
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Volunteer" colon={false}>
        {getFieldDecorator(
          'Volunteers',
          {
            initialValue: recordData.volunteerId
          },
          {
            rules: [{ required: true, message: 'Please enter role' }]
          }
        )(
          <Select>
            {volunteers.map(volunteer => (
              <Select.Option key={volunteer.id} value={volunteer.name}>
                {volunteer.name}
              </Select.Option>
            ))}
          </Select>
        )}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator(
          'email',
          {
            initialValue: recordData.email || null
          },
          {
            rules: [{ required: true, message: 'Please enter role' }]
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="PhoneNumber" colon={false}>
        {getFieldDecorator(
          'phoneNumber',
          {
            initialValue: recordData.phoneNumber
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date')
              }
            ]
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Start Work Date" colon={false}>
        {getFieldDecorator(
          'desc',
          {
            initialValue: moment(recordData.dayInCompany)
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date')
              }
            ]
          }
        )(<DatePicker />)}
      </Form.Item>
      <Form.Item label="Avatar" colon={false}>
        {getFieldDecorator('profilePic', {
          initialValue: {
            status: 'done',
            uid: recordData.imgName
          }
        })(
          <Upload name="logo" customRequest={handleUpload} listType="picture">
            <Button>
              <Icon type="upload" />
            </Button>
          </Upload>
        )}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.edit')}
        </Button>
      </Form.Item>
    </Form>
  );
};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
  volunteers: state.volunteers.volunteers
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  saveDraft: params => dispatch(saveDraftAction(params)),
  uploadImage: (file, onSuccess) =>
    dispatch(uploadImageAction(file, onSuccess)),
  getAllVolunteers: () => dispatch(getAllTransactionsAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Edit));
