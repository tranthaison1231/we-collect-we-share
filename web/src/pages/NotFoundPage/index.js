import React from 'react';
import NotFoundPageWrapper from './styles';
import { NotFoundIcon } from "../../assets/icons";

function NotFoundPage() {
  return (
    <NotFoundPageWrapper>
      <div className="main">
        <NotFoundIcon />
        <div className="text-div">
          <div>404</div>
          <div>Page not found</div>
        </div>
      </div>
    </NotFoundPageWrapper>
  );
}

export default NotFoundPage;
