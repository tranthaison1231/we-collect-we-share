import React, { useEffect } from 'react';
import { Form, Button, Input, DatePicker } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  closeModalAction,
  saveDraftAction
} from '../../../redux/modal/actions';
import { editVolunteerAction } from '../../../redux/volunteers/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  recordData,
  editVolunteer,
  id,
  draft,
  closeModal,
  saveDraft,
}) => {
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        const birthDay = new Date(values.birthDay).getTime();
        editVolunteer({
          id: recordData.id,
          employee: {
            name: values.name,
            job: values.job,
            email: values.email,
            birthDay,
            address: values.address,
            phoneNumber: values.phoneNumber           
          }
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <Form onSubmit={onSubmit} className="login-form">
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator(
          'name',
          {
            initialValue: recordData.name || null
          },
          {
            rules: [{ required: true, message: 'Please enter role' }]
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator(
          'email',
          {
            initialValue: recordData.email || null
          },
          {
            rules: [{ required: true, message: 'Please enter role' }]
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Birthday" colon={false}>
        {getFieldDecorator(
          'birthDay',
          {
            initialValue: moment(recordData.birthDay)
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date')
              }
            ]
          }
        )(<DatePicker />)}
      </Form.Item>
      <Form.Item label="Number Phone" colon={false}>
        {getFieldDecorator(
          'phoneNumber',
          {
            initialValue: recordData.phoneNumber || null
          },
          {
            rules: [{ required: true, message: 'Please enter role' }]
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Address" colon={false}>
        {getFieldDecorator(
          'address',
          {
            initialValue: recordData.address || null
          },
          {
            rules: [{ required: true, message: 'Please enter role' }]
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.edit')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Edit.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  editVolunteer: PropTypes.func,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  recordData: PropTypes.object,
};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  editVolunteer: params => dispatch(editVolunteerAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Edit));
