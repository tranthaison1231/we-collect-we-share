import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import LoadingComponent from '../components/common/LoadingScreenTransparent';
import { changeLoading } from '../redux/loading/actions';

const useFetchData = ({ action, state }) => {
  const dispatch = useDispatch();
  const params = useParams();
  const data = useSelector(state);

  useEffect(() => {
    async function fetchData() {
      try {
        dispatch(changeLoading(true));
        await dispatch(action(params.id));
        dispatch(changeLoading(false));
      } catch (e) {
        dispatch(changeLoading(false));
        console.error(e);
      }
    }
    fetchData();
  }, [action, dispatch, params.id]);
  return [data];
};

const withFetchDataById = params => Component => props => {
  const isLoading = useSelector(state => state.loading.isLoading);
  const [data] = useFetchData({
    ...params,
  });
  return isLoading ? (
    <LoadingComponent />
  ) : (
    <Component {...props} data={data} />
  );
};

export default withFetchDataById;
