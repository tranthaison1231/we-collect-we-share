import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
//
import ListView from '../ListView';
import Detail from '../Detail';
import MyList from '../MyList';

const ListStack = createStackNavigator({
  ListView,
  Detail,
  MyList,
});

export const Root = createAppContainer(ListStack);
