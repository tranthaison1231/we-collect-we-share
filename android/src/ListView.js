/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ActivityIndicator,
  StatusBar,
  Dimensions,
} from 'react-native';
import firebase from 'react-native-firebase';
import ic_address from './images/address.png';
import ic_text from './images/text.png';
import ic_avatar from './images/avatar.png';
import ic_list from './images/list.png';

const width = Dimensions.get('window').width;

const ListView = props => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    const ref = firebase
      .firestore()
      .collection('transactions')
      .where('status', '==', 'pending');
    const getData = async () => {
      await ref.onSnapshot({ includeMetadataChanges: true }, onSnapshot => {
        let arr = [];
        console.log('onSnapshot', onSnapshot);
        onSnapshot.forEach(doc => {
          arr.push({ id: doc.id, data: doc.data() });
        });

        setData(arr);
        setLoading(false);
      });
    };
    setLoading(true);
    getData();
  }, []);

  const goToDetail = item => () => {
    props.navigation.navigate('Detail', { item, title: item.data.giverName });
  };

  const item = ({ item }) => (
    <TouchableOpacity
      style={{
        flex: 1,
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10,
        padding: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#aaa',
      }}
      onPress={goToDetail(item)}>
      <Image
        source={ic_avatar}
        style={{ width: 30, height: 30, borderRadius: 15, marginRight: 15 }}
      />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: width * 0.5,
        }}>
        <View>
          <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 5 }}>
            {item.data.giverName}
          </Text>
          <View
            style={{
              width: 150,
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 5,
            }}>
            <Image
              source={ic_address}
              style={{ width: 18, height: 18, marginRight: 10 }}
            />
            <Text>{item.data.address}</Text>
          </View>
          <View
            style={{
              width: 150,
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 5,
            }}>
            <Image
              source={ic_text}
              style={{ width: 18, height: 18, marginRight: 10 }}
            />
            <Text numberOfLines={2} ellipsizeMode={'tail'}>
              {item.data.description}
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          height: 40,
          paddingHorizontal: 15,
          paddingVertical: 10,
          backgroundColor: '#f5522d',
          borderRadius: 20,
        }}>
        <Text style={{ color: '#fff', fontWeight: 'bold' }}>Pending</Text>
      </View>
    </TouchableOpacity>
  );

  const listEmptyComponent = () => (
    <View style={{ flex: 1, alignItems: 'center', marginTop: 40 }}>
      <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
        Danh sách công việc trống
      </Text>
    </View>
  );

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator color={'#43a047'} size={'large'} />
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <StatusBar backgroundColor="#00701a" barStyle="light-content" />
      <FlatList
        renderItem={item}
        data={data}
        ListEmptyComponent={listEmptyComponent}
        keyExtractor={(item, index) => `${index}`}
      />
    </View>
  );
};

ListView.navigationOptions = ({ navigation }) => ({
  title: 'List ',
  headerStyle: {
    backgroundColor: '#43a047',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  headerRight: (
    <TouchableOpacity
      style={{ margin: 10 }}
      onPress={() => navigation.navigate('MyList')}>
      <Image source={ic_list} style={{ width: 36, height: 36 }} />
    </TouchableOpacity>
  ),
});

export default ListView;
