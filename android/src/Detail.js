/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Linking,
  Dimensions,
} from 'react-native';
import MapView from 'react-native-maps';
import ic_address from './images/address.png';
import ic_text from './images/text.png';
import ic_phone from './images/phone.png';

import firebase from 'react-native-firebase';

const width = Dimensions.get('window').width;

const Detail = props => {
  const [item, setItem] = useState(props.navigation.getParam('item') || {});

  // const onRegionChange = region => {
  //   console.log(region);

  //   // this.setState({ region });
  // };

  const updateTransaction = () => {
    if (item.data && item.data.status === 'pending') {
      const ref = firebase
        .firestore()
        .collection('transactions')
        .doc(item.id);

      firebase
        .firestore()
        .runTransaction(async transaction => {
          const newDoc = {
            ...item.data,
            status: 'processing',
            volunteerId: '0KyuDVvkVvjGvTDjX9qO',
          };
          transaction.update(ref, newDoc);
          return newDoc;
        })
        .then(updatedItem => {
          setItem({ id: item.id, data: { ...updatedItem } });
        })
        .catch(error => {});
    }
  };

  const openMap = () => {
    const query = `${item.data.lat},${item.data.lng}`;
    Linking.openURL(`https://www.google.com/maps/search/?api=1&query=${query}`);
  };

  const renderInfo = () => (
    <View
      style={{
        flexDirection: 'row',
        margin: 10,
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
        justifyContent: 'space-between',
      }}>
      <View style={{ width: width * 0.5 }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_address}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text>{(item.data && item.data.address) || ''}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_text}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text>{(item.data && item.data.description) || ''}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_phone}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text>{(item.data && item.data.phoneNumber) || ''}</Text>
        </View>
      </View>
      <Text
        style={{
          height: 40,
          color: '#fff',
          fontWeight: 'bold',
          paddingVertical: 10,
          paddingHorizontal: 15,
          borderRadius: 20,
          backgroundColor:
            item.data && item.data.status === 'pending' ? '#f5522d' : '#43a047',
        }}>
        {(item.data && item.data.status) || ''}
      </Text>
    </View>
  );

  const renderImages = () =>
    item.data &&
    item.data.imageURL &&
    item.data.imageURL.length > 0 && (
      <View
        style={{
          padding: 10,
          margin: 10,
          marginTop: 0,
          backgroundColor: '#fff',
          borderRadius: 10,
        }}>
        <ScrollView horizontal={true}>
          {item.data.imageURL &&
            item.data.imageURL.map((uri, index) => (
              <Image
                source={{ uri }}
                style={{ width: 100, height: 100, marginRight: 10 }}
                key={`${index}`}
              />
            ))}
        </ScrollView>
      </View>
    );

  console.log('renderMap', item);

  const renderMap = () => (
    <View style={{ flex: 7, backgroundColor: '#fff', paddingBottom: 10 }}>
      <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 10 }}>
        Chỉ Đường
      </Text>
      <View
        style={{
          width: '95%',
          alignSelf: 'center',
          height: 300,
        }}>
        {item.data && item.data.lat && item.data.lng && (
          <MapView
            style={{ flex: 1 }}
            region={{
              latitude: parseFloat(item.data.lat),
              longitude: parseFloat(item.data.lng),
              latitudeDelta: 0.09922,
              longitudeDelta: 0.09421,
            }}>
            <MapView.Marker
              coordinate={{
                latitude: parseFloat(item.data.lat),
                longitude: parseFloat(item.data.lng),
              }}
              title={item.data.giverName}
              onPress={openMap}
            />
          </MapView>
        )}
      </View>
    </View>
  );

  const renderButton = () =>
    item.data &&
    item.data.status === 'pending' && (
      <View
        style={{
          position: 'absolute',
          width: '100%',
          bottom: 10,
        }}>
        <TouchableOpacity
          onPress={updateTransaction}
          style={{
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            width: 200,
            height: 40,
            marginVertical: 10,
            borderRadius: 20,
            elevation: 1,
            backgroundColor: '#43a047',
          }}>
          <Text style={{ color: '#fff', fontWeight: 'bold' }}>
            {item.data.status === 'pending'
              ? 'TAKE JOB'
              : `${item.data.status}`}
          </Text>
        </TouchableOpacity>
      </View>
    );

  return (
    <View style={{ flex: 1, backgroundColor: '#efefef' }}>
      <ScrollView contentContainerStyle={{ paddingBottom: 30 }}>
        {renderInfo()}
        {renderImages()}
        {renderMap()}
      </ScrollView>
      {renderButton()}
    </View>
  );
};

Detail.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title'),
  headerStyle: {
    backgroundColor: '#43a047',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

export default Detail;
