/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native';
import firebase from 'react-native-firebase';
import ic_address from './images/address.png';
import ic_text from './images/text.png';
import ic_avatar from './images/avatar.png';

const MyList = props => {
  const [loading, setLoading] = useState(false);

  const [data, setData] = useState([]);

  useEffect(() => {
    const ref = firebase.firestore().collection('transactions');
    const getData = async () => {
      await ref.onSnapshot({ includeMetadataChanges: true }, onSnapshot => {
        let arr = [];
        console.log('onSnapshot', onSnapshot);
        onSnapshot.forEach(doc => {
          if (doc.data().status !== 'pending') {
            arr.push({
              id: doc.id,
              data: doc.data(),
            });
          }
        });

        setData(arr);
        setLoading(false);
      });
    };
    setLoading(true);
    getData();
  }, []);

  const goToDetail = item => () => {
    props.navigation.navigate('Detail', { item, title: item.data.giverName });
  };

  const item = ({ item }) => (
    <TouchableOpacity
      style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}
      onPress={goToDetail(item)}>
      <View
        style={{
          flex: 4,
          flexDirection: 'row',
          justifyContent: 'space-between',
          margin: 10,
          padding: 10,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: '#aaa',
        }}>
        <Image
          source={ic_avatar}
          style={{ width: 30, height: 30, borderRadius: 15, marginRight: 15 }}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 5 }}>
              {item.data.giverName}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 5,
              }}>
              <Image
                source={ic_address}
                style={{
                  width: 18,
                  height: 18,
                  marginRight: 10,
                }}
              />
              <Text number={2}>{item.data.address}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 5,
              }}>
              <Image
                source={ic_text}
                style={{ width: 18, height: 18, marginRight: 10 }}
              />
              <Text>{item.data.description}</Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          width: 80,
          marginTop: 40,
          height: 40,
          backgroundColor:
            item.data.status === 'success' ? '#43a047' : '#039be5',
          borderRadius: 10,
        }}>
        <Text style={{ color: '#fff' }}>{item.data.status}</Text>
      </View>
    </TouchableOpacity>
  );

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator color={'#43a047'} size={'large'} />
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        renderItem={item}
        data={data}
        keyExtractor={(item, index) => `${index}`}
      />
    </View>
  );
};

MyList.navigationOptions = {
  title: 'My List',
  headerStyle: {
    backgroundColor: '#43a047',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

export default MyList;
